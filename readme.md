# Simple boilerplate frontend con pug, sass and babel.

<img src="https://hsto.org/webt/dl/eq/ic/dleqicf-sk-seha4s06qmvgzje4.png" style="float: right">

## Documentación en español

 Simple boilerplate para trabajar con pug, sass y babel usando nodejs y un archivo de configuración packaje.json
 
### Instrucciones
 
1. Abrir una terminal y escribir `git clone https://gitlab.com/LeonEmil/boilerplate-static.git` en la carpeta que queremos clonar el proyecto (recomiendo hacer un fork del proyecto en su cuenta de gitlab)
2. Ir a la carpeta desde la terminal y escribir `npm install` para descargar los módulos
3. Abrir la carpeta del proyecto desde un editor e ir a node_modules/parallelshell/index.js y cambiar la siguiente linea:

    `cwd: process.versions.node < '8.0.0' ? process.cwd : process.cwd(),`

    Por la siguiente:
    
    `cwd: parseInt(process.versions.node) < 8 ? process.cwd : process.cwd(),`
    
4. Ejecutar `npm start`. Los archivos serán compilados y se abrirá un servidor sincronizado con los cambios que hagamos al proyecto.

<hr>

## English documentation

 This is a simple boilerplate for work with pug, sass and babel with nodejs and a config file package.json.
 
 ### Instructions

1. Open a terminal and type `git clone https://gitlab.com/LeonEmil/boilerplate-static.git` in the folder we want to clone the project
2. Go to the folder from the terminal and type `npm install` to download the modules
3. Open the project folder from an editor and go to node_modules/parallelshell/index.js and change the following line:

`cwd: process.versions.node '8.0.0' ? process.cwd : process.cwd(),`

To the following text:

`cwd: parseInt(process.versions.node) < 8 ? process.cwd : process.cwd(),`

4. Run `npm start`. The files will be compiled and a server will open synchronized with the changes we make to the project.